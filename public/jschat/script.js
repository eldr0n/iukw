let username;
let chars = false;

function validate() {
    username = document.getElementById("inputUsername").value;
    if (username.length >= 3) {
        chars = true;
    } else {
        chars = false;
    }
}

function login() {
    if (chars === true) {
        window.location.href = "list.html"
    } else {
        alert("min. 3 chars!");
    }
}

function toChat() {
    window.location.href = "chat.html"
}

function send() {
    let message = document.getElementById("message").value;

    message = message.replace("home", '<span class="icon">home</span>');
    message = message.replace("cake", '<span class="icon">cake</span>');

    let text = document.createTextNode(message);
    let paragraph = document.createElement("p");

    paragraph.className = "chat-self";
    paragraph.appendChild(text);

    document.getElementById("chat").appendChild(paragraph);

}