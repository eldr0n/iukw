function createCard(name, birth_year, url) {
    let divCol = document.createElement("div");
    divCol.className = "col-sm";

    let divCard = document.createElement("div");
    divCard.className = "card";

    let divCardBody = document.createElement("div");
    divCardBody.className = "card-body";

    let cardTitle = document.createElement("h4");
    cardTitle.className = "card-title";
    cardTitle.innerHTML = name;

    let cardSubtitle = document.createElement("h4");
    cardSubtitle.className = "card-subtitle mb-2 text-muted";
    cardSubtitle.innerHTML = birth_year;

    let world = document.createElement("p");
    world.style.display = "none";

    let button = document.createElement("button");
    button.className = "btn btn-dark";
    button.innerHTML = "Heimat anzeigen";
    button.onclick = function () {
        button.style.display = "none";
        home(url, world)
    };

    divCardBody.appendChild(cardTitle);
    divCardBody.appendChild(cardSubtitle);
    divCardBody.appendChild(world);
    divCardBody.appendChild(button);

    divCard.appendChild(divCardBody);
    divCol.appendChild(divCard);

    document.getElementById("people").appendChild(divCol);
}

let people = "https://swapi.co/api/people/";
fetch(people)
    .then(res => res.json())
    .then(function (data) {
        let object = data.results;
        for (let i = 0; i <= object.length; i++) {
            createCard(object[i].name, object[i].birth_year, object[i].homeworld);
        }
    });

function home(url, world) {
    fetch(url)
        .then(res => res.json())
        .then(function (data) {
            world.innerHTML = "Heimat: " + data.name;
            world.style.display = "inline";
        });
}
